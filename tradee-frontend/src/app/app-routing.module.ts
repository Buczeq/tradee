import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ResetPasswordComponent } from './components/login/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/login/change-password/change-password.component';
import { TradeComponent } from './components/trade/trade.component';
import { ConfirmMailComponent } from './components/login/confirm-mail/confirm-mail.component';
import { SingleTradeComponent } from './components/trade/single-trade/single-trade.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ExploreTradesComponent } from './components/trade/explore-trades/explore-trades.component';


const routes: Routes = [
    {
    path: 'trade/explore-trades',
    component: ExploreTradesComponent
    },
  {
    path: 'home',
    component: HomeComponent
},
{
    path: 'user',
    component: UserComponent
},
{
    path: 'admin',
    component: AdminComponent
},
{
    path: 'auth/login',
    component: LoginComponent
},
{
    path: 'auth/reset-password',
    component: ResetPasswordComponent
},
{
    path: 'auth/change-password',
    component: ChangePasswordComponent
},
{
    path: 'auth/confirm-mail',
    component: ConfirmMailComponent
},
{
    path: 'signup',
    component: RegisterComponent
},
{
    path: 'create-trade',
    component: TradeComponent
},
{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
},
{
    path: 'trade/single-trade',
    component: SingleTradeComponent
},
{
    path: 'about-us',
    component: AboutUsComponent
},
{
    path: 'trades',
    component: ExploreTradesComponent
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
