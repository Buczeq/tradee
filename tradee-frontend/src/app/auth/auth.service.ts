import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { JwtResponse } from './jwt-response';
import { AuthLoginInfo } from './login-info';
import { SignUpInfo } from './signup-info';
import { AppSettings } from '../app-settings';
import { EmailInfo } from '../components/login/reset-password/email-info';
import { ChangePasswordInfo } from '../components/login/change-password/change-password-info';
import { NewPasswordInfo } from '../components/login/change-password/new-password-info';
import { TokenStorageService } from './token-storage.service';
import { ConfirmMailInfo } from '../components/login/confirm-mail/confirm-mail-info';
import { ResendConfirmMailInfo } from '../components/login/confirm-mail/resend-confirm-mail-info';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string;
  private loginUrl = AppSettings.API_ENDPOINT + '/auth/signin';
  private signupUrl = AppSettings.API_ENDPOINT + '/auth/signup';
  // private savePasswordUrl = AppSettings.API_ENDPOINT + '/auth/save-password';
  private changePasswordUrl = AppSettings.API_ENDPOINT + '/auth/change-password';
  private resetPasswordUrl = AppSettings.API_ENDPOINT + '/auth/reset-password';
  private confirmMailUrl = AppSettings.API_ENDPOINT + '/auth/confirm-mail';
  private resendConfirmMailUrl = AppSettings.API_ENDPOINT + '/auth/resend-confirm-mail';

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }
  attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  signUp(info: SignUpInfo): Observable<string> {
    this.http.get<string>(AppSettings.API_ENDPOINT + '/auth/getcsrf', httpOptions).subscribe(data => {
     console.log(data);
    });
    return this.http.post<string>(this.signupUrl, info, httpOptions);
  }

  resetPassword(emailInfo: EmailInfo): Observable<string> {
    return this.http.post<string>(this.resetPasswordUrl, emailInfo, httpOptions);
  }

  changePassword(changePasswordInfo: ChangePasswordInfo, newPasswordInfo: NewPasswordInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.changePasswordUrl + "?id=" + changePasswordInfo.id + "&token=" + changePasswordInfo.token, newPasswordInfo, httpOptions);
  }

  confirmMail(confirmMailInfo: ConfirmMailInfo): Observable<string> {
    return this.http.post<string>(this.confirmMailUrl +"?token=" + confirmMailInfo.token, httpOptions);
  }

  // now this is usless but when sb wants to confrim account after expirtation
  // He could do it here
  resendConfirmMail(resendConfirmMailInfo: ResendConfirmMailInfo): Observable<string> {
    return this.http.post<string>(this.resendConfirmMailUrl + '?email=' + resendConfirmMailInfo.email, httpOptions);
  }


}
