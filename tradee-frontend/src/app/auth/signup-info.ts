export class SignUpInfo {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    phone: number;
 
    constructor(firstname: string, lastname: string, email: string, password: string, phone: number) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }
}