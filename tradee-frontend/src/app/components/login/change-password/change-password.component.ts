import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { ChangePasswordInfo } from './change-password-info';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { NewPasswordInfo } from './new-password-info';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  errorMessage = '';
  private changePasswordInfo: ChangePasswordInfo;
  private newPasswordInfo: NewPasswordInfo;
  form: any = {};
  isPasswordResetFailed = false;
  isLoggedIn = false;
  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute) {
    

  }
  ngOnInit() {

  }


  onSubmit() {
 
    this.newPasswordInfo = new NewPasswordInfo(
      this.form.newPassword);
      
      this.activatedRoute.queryParams.subscribe(params => {
        this.changePasswordInfo = new ChangePasswordInfo(params['id'], params['token']);
      });
    this.authService.changePassword(this.changePasswordInfo, this.newPasswordInfo).subscribe(
      data => { console.log(data)
        this.reloadPage();
        // here instead of reloading page, route it to login page
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isPasswordResetFailed = true;
      }
    );
  }


  
  reloadPage() {
    window.location.reload();
  }
}
