import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { ConfirmMailInfo } from './confirm-mail-info';

@Component({
  selector: 'app-confirm-mail',
  templateUrl: './confirm-mail.component.html',
  styleUrls: ['./confirm-mail.component.css']
})
export class ConfirmMailComponent implements OnInit {
  errorMessage = '';
  private confirmMailInfo: ConfirmMailInfo;
  
  isConfirmMailFailed = false;
  constructor(private authService: AuthService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.confirmMailInfo = new ConfirmMailInfo(params['token']);
    });
  }
confirmMail(){
  this.authService.confirmMail(this.confirmMailInfo).subscribe(
    data => { console.log(data)
      this.reloadPage();
      // here instead of reloading page, route it to home page
    },
    error => {
      console.log(error);
      this.errorMessage = error.error.message;
      this.isConfirmMailFailed = true;
    }
  );
  }
  reloadPage() {
    window.location.reload();
  }
}
