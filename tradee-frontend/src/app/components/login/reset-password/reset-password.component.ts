import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { EmailInfo } from './email-info';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  form: any = {};
  
  isPasswordResetFailed = false;
  errorMessage = '';
  isLoggedIn = false;
  roles: string[] = [];
  private emailInfo: EmailInfo;
  constructor(private authService: AuthService, private tokenStorage: TokenStorageService) { }

  ngOnInit() { }

  onSubmit() {
    console.log(this.form);
 
    this.emailInfo = new EmailInfo(
      this.form.email);

    this.authService.resetPassword(this.emailInfo).subscribe(
      data => { 
        console.log(data);
        this.reloadPage();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isPasswordResetFailed = true;
      }
    );
  }
 
  reloadPage() {
    window.location.reload();
  }

}
