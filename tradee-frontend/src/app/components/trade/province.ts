import { County } from './county';

export class Province {
    name: string;
    provinceId: number;
    counties: County[];
    constructor(name: string, provinceId: number, counties: County[]){
        this.name = name;
        this.provinceId = provinceId;
        this.counties = counties;
    }
}