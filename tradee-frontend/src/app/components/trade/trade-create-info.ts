import { Category } from './category';
import { County } from './county';
import { ImageNameInfo } from './image-name-info';

export class TradeCreateInfo {
    title: string;
    description: string;
    size: string;
    sex: string;
    categoryList: Category[];
    countyId: County;
    imageNames: ImageNameInfo[];

    constructor(
        title: string,
        description: string,
        size: string,
        sex: string,
        categoryList: Category[],
        countyId: County,
        imageNames: ImageNameInfo[]
    ) {
this.title = title;
this.description = description;
this.size = size;
this.sex = sex;
this.categoryList = categoryList;
this.countyId = countyId;
this.imageNames = imageNames;
    }
}