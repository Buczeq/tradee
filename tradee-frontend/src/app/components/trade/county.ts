export class County {
    name: string;
    countyId: number;
    constructor(name: string, countyId: number){
        this.name = name;
        this.countyId = countyId;
    }
}