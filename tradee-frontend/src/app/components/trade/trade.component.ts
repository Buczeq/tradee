import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TradeService } from 'src/app/services/trade.service';
import { FileUploader } from "ng2-file-upload";
import { Province } from './province';
import { County } from './county';
import { Category } from './category';
import { TradeCreateInfo } from './trade-create-info';
import { ImageNameInfo } from './image-name-info';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/app-settings';
import { v4 as uuid } from 'uuid';
@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {
  form: any = {};
  uploadForm: FormGroup;

  public uploader: FileUploader = new FileUploader({
    isHTML5: true
  });
  title: string = 'Angular File Upload';

  availableSex = [
    { name: "Męskie", label: "MALE" },
    { name: "Damskie", label: "FEMALE" },
    { name: "Unisex", label: "UNISEX" },
  ]
  availableProvinces: Province[];
  availableCounties: County[];
  availableCategories: Category[];
  tradeCreateInfo: TradeCreateInfo;
  isTradeCreated = false;
  isTradeCreatedFailed = false;
  errorMessage = '';
  images: Array<ImageNameInfo> = new Array<ImageNameInfo>();
  constructor(private fb: FormBuilder, private http: HttpClient, private tradeService: TradeService) { }

  onSubmit() {
    for (let i = 0; i < this.uploader.queue.length; i++) {
      let fileItem = this.uploader.queue[i]._file;
      if (fileItem.size > 10000000) {
        alert("Each File should be less than 10 MB of size.");
        return;
      }
    }
    for (let j = 0; j < this.uploader.queue.length; j++) {
      let data = new FormData();
      let fileItem = this.uploader.queue[j]._file;
      data.append('file', fileItem);
      data.append('fileSeq', 'seq' + j);
      data.append('dataType', "TradeImage");
      let uuidToken: string = uuid();
      // data.append('newName', newName)
      let imageNameInfo = new ImageNameInfo(uuidToken + fileItem.name);
      this.images.push(imageNameInfo);
      this.tradeService.uploadImages(data, uuidToken).subscribe(response => {
        console.log(response);
      });
    }

    this.uploader.clearQueue();
    let categoriesDto = new Array<Category>();
categoriesDto.push(this.form.categories);
    this.tradeCreateInfo = new TradeCreateInfo(
      this.form.title,
      this.form.description,
      this.form.size,
      this.form.sex,
      categoriesDto,
      this.form.county,
      this.images
    )

    this.tradeService.createTrade(this.tradeCreateInfo).subscribe(
      data => {
        this.isTradeCreated = true;
        this.isTradeCreatedFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isTradeCreatedFailed = true;
      }
    );
  }


  updateCounties(province) {
    this.availableCounties = province.counties;
  }


  ngOnInit() {
    this.uploadForm = this.fb.group({
      document: [null, null],
      type: [null, Validators.compose([Validators.required])]
    });

    this.tradeService.getProvinces().subscribe(data => {
      this.availableProvinces = data;
      this.availableCounties = this.availableProvinces[0].counties;
    });
    this.tradeService.getCategories().subscribe(data => {
      this.availableCategories = data;
    });
  }

}
