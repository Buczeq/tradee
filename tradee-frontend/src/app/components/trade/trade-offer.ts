import { Category } from './category';
import { County } from './county';

export class TradeOffer {
    tradeOfferId: number;
    title: string;
    description: string;
    size: string;
    sex: string;
    categoryList: Array<Category>;
    countyId: County;
    imageCollection: Array<string>;
}