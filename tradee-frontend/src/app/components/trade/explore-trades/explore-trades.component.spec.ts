import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreTradesComponent } from './explore-trades.component';

describe('ExploreTradesComponent', () => {
  let component: ExploreTradesComponent;
  let fixture: ComponentFixture<ExploreTradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploreTradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreTradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
