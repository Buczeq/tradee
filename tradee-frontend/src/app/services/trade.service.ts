import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppSettings } from '../app-settings';
import { TokenStorageService } from '../auth/token-storage.service';
import { Observable } from 'rxjs';
import { Province } from '../components/trade/province';
import { Category } from '../components/trade/category';
import { TradeCreateInfo } from '../components/trade/trade-create-info';
import { ImageNameInfo } from '../components/trade/image-name-info';
import { TradeOffer } from '../components/trade/trade-offer';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json' })
};

const httpOptionsFile = {
  headers: new HttpHeaders({
    'Content-Type': 'multipart/form-data' })
};
@Injectable({
  providedIn: 'root'
})
export class TradeService {
  private uploadImagesUrl = AppSettings.API_ENDPOINT + '/trade/upload-images';
  private createTradeUrl = AppSettings.API_ENDPOINT + '/trade/create-trade';
  private categoriesUrl = AppSettings.API_ENDPOINT + '/trade/categories';
  private provincesUrl = AppSettings.API_ENDPOINT + '/trade/provinces';
  private addToWishListUrl = AppSettings.API_ENDPOINT + '/trade/add-to-wishlist';
  private getWishedTradesListUrl = AppSettings.API_ENDPOINT + '/trade/get-wished-trades';
  private getMineWishedTradesListUrl = AppSettings.API_ENDPOINT + '/trade/get-my-wished-trades';

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }
  uploadImages(datas: FormData, uuidToken: string): Observable<ImageNameInfo> {
    return this.http.post<ImageNameInfo>(this.uploadImagesUrl + "?uuid=" + uuidToken, datas);
  } 


  getProvinces(): Observable<Province[]> {
    return this.http.get<Province[]>(this.provincesUrl, httpOptions);
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoriesUrl, httpOptions);
  }

  createTrade(tradeCreateInfo: TradeCreateInfo): Observable<string> {
    return this.http.post<string>(this.createTradeUrl, tradeCreateInfo, httpOptions);
  }

  addToWishList(tradeOfferId: string): Observable<string> {
  return this.http.post<string>(this.addToWishListUrl + "?trade-offer-id=" + tradeOfferId, httpOptions);
  }


  getWishedTrades(tradeOfferId: string): Observable<TradeOffer> {
    return this.http.get<TradeOffer>(this.addToWishListUrl + "?trade-offer-id=" + tradeOfferId, httpOptions);
    }


}
