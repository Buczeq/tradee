import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from '../app-settings';
 
@Injectable({
  providedIn: 'root'
})
export class UserService {
 
  private userUrl =  AppSettings.API_ENDPOINT + '/test/user';
  private adminUrl =  AppSettings.API_ENDPOINT + '/test/admin';
 
  constructor(private http: HttpClient) { }
 

}