import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { AdminComponent } from './components/admin/admin.component';
import { httpInterceptorProviders } from './auth/auth-interceptor';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordComponent } from './components/login/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/login/change-password/change-password.component';
import { TradeComponent } from './components/trade/trade.component';
import { RouterModule } from '@angular/router';
import { FileSelectDirective, FileUploadModule } from 'ng2-file-upload';
import { CustomMaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExploreTradesComponent } from './components/trade/explore-trades/explore-trades.component';
import { ConfirmMailComponent } from './components/login/confirm-mail/confirm-mail.component';
import { SingleTradeComponent } from './components/trade/single-trade/single-trade.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserComponent,
    AdminComponent,
    AboutUsComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    TradeComponent,
    ExploreTradesComponent,
    ConfirmMailComponent,
    SingleTradeComponent,
    AboutUsComponent
    // FileSelectDirective
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CustomMaterialModule,
    FileUploadModule ,
    BrowserAnimationsModule,
    // HttpClientXsrfModule.withOptions({cookieName: 'XSRF-TOKEN', headerName: 'XSRF-TOKEN'})
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
