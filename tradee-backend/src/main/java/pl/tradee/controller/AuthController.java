/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import pl.tradee.auth.jwt.JwtProvider;
import pl.tradee.message.request.ChangePasswordForm;
import pl.tradee.message.request.EmailDto;
import pl.tradee.model.Role;
import pl.tradee.model.RoleName;
import pl.tradee.repository.RoleRepository;
import pl.tradee.repository.UserAccountRepository;
import pl.tradee.message.request.LoginForm;
import pl.tradee.message.request.PasswordDto;
import pl.tradee.message.response.ResponseMessage;
import pl.tradee.message.request.SignUpForm;
import pl.tradee.message.response.JwtResponse;
import pl.tradee.model.PasswordResetToken;
import pl.tradee.model.UserAccount;
import pl.tradee.model.UserPrinciple;
import pl.tradee.model.VerificationToken;
import pl.tradee.repository.PasswordResetTokenRepository;
import pl.tradee.repository.VerificationTokenRepository;
import pl.tradee.serviceImpl.AuthServiceImpl;
import pl.tradee.serviceImpl.EmailServiceImpl;

/**
 *
 * @author Tomek
 */
@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserAccountRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    EmailServiceImpl emailService;

    @Autowired
    AuthServiceImpl authService;

    /**
     * Authenticates user if username and password are valid and generates JSON Web Token
     * @param loginRequest form with username and password
     * @return response containing JSON Web Token
     */
    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }

    /**
     * Creates new user account
     * @param signUpRequest form with user's data
     * @return response informing whether user was registered successfully
     */
    @Transactional
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        authService.registerNewUser(signUpRequest);

        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }

    /**
     * Generates passwors reset token ang sends it in e-mail
     * @param email e-mail data transfer object
     * @return response informing if e-mail was sent successfully and reason if not
     */
    @PostMapping("/reset-password")
    public ResponseEntity<?> constructResetTokenEmail(@Valid @RequestBody EmailDto email) {
        if (!userRepository.existsByEmail(email.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> This email is not exists!"),
                    HttpStatus.BAD_REQUEST);
        }

        PasswordResetToken resetToken = authService.generateResetPasswordToken(email.getEmail());
        authService.generateChangePasswordMail(email.getEmail(), resetToken);

        return new ResponseEntity<>(new ResponseMessage("Link has been sent to your email!"), HttpStatus.OK);
    }

    /**
     * Allows user to reset password if token is valid 
     * @param id user's id
     * @param token password reset token
     * @param newPassword new password to set
     * @return response informing if user can change password and reason if not
     */
    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@Valid @RequestBody @RequestParam("id") long id, @RequestParam("token") String token, @RequestBody PasswordDto newPassword) {

        PasswordResetToken passToken = passwordResetTokenRepository.findByToken(token).get();

        if ((passToken == null) || (passToken.getUserId().getUserId() != id)) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Invalid Token!"),
                    HttpStatus.BAD_REQUEST);
        }

        Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate()
                .getTime() - cal.getTime()
                        .getTime()) <= 0) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Token expired!"),
                    HttpStatus.BAD_REQUEST);
        }

        authService.changeUserPassword(passToken.getUserId(), newPassword.getNewPassword());
        passToken.setExpiryDate(Calendar.getInstance().getTime());
        passwordResetTokenRepository.save(passToken);

        return new ResponseEntity<>(new ResponseMessage("Password changed successfully!"), HttpStatus.OK);
    }

    /**
     * Activates user's account if token is valid
     * @param token activation token sent in e-mail
     * @return response informing if account is activatest and reason if not
     */
    @PostMapping("/confirm-mail")
    public ResponseEntity<?> confirmMail(@Valid @RequestParam("token") String token) {
        if (!verificationTokenRepository.existsByToken(token)) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> There is no such token!"),
                    HttpStatus.BAD_REQUEST);
        }

        VerificationToken verificationToken = verificationTokenRepository.findByToken(token)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Token not find."));

        if (!verificationToken.getExpiryDate().after(new Date())) {
            return new ResponseEntity<>(new ResponseMessage("Verification token has expired!"), HttpStatus.FORBIDDEN);
        }

        authService.confirmMail(verificationToken);

        return new ResponseEntity<>(new ResponseMessage("User's mail confirmed successfully!"), HttpStatus.OK);
    }

    /**
     * Resends activation e-mail
     * @param email e-mail address
     * @return response informing if e-mail was sent successfully
     */
    @PostMapping("/resend-confirm-mail")
    public ResponseEntity<?> resendConfirmMailLink(@Valid @RequestParam("email") String email) {
        authService.resendConfirmMailLink(email);

        return new ResponseEntity<>(new ResponseMessage("Confirmation e-mail sent successfully!"), HttpStatus.OK);
    }

}
