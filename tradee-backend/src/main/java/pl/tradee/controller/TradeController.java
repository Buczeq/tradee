/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.tradee.message.response.ImageNameInfo;
import pl.tradee.message.response.ResponseMessage;
import pl.tradee.model.Category;
import pl.tradee.model.CategoryDto;
import pl.tradee.model.County;
import pl.tradee.model.Image;
import pl.tradee.model.ProvinceDto;
import pl.tradee.model.TradeOffer;
import pl.tradee.model.TradeOfferDto;
import pl.tradee.model.UserAccount;
import pl.tradee.model.UserPrinciple;
import pl.tradee.repository.CategoryRepository;
import pl.tradee.repository.CountyRepository;
import pl.tradee.repository.ImageRepository;
import pl.tradee.repository.TradeOfferRepository;
import pl.tradee.serviceImpl.AuthServiceImpl;
import pl.tradee.serviceImpl.TradeServiceImpl;

/**
 *
 * @author Tomek
 */
@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/trade")
public class TradeController {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CountyRepository countyRepository;
    @Autowired
    TradeOfferRepository tradeOfferRepository;
    @Autowired
    AuthServiceImpl authService;
    @Autowired
    ImageRepository imageRepository;

    @Autowired
    TradeServiceImpl tradeService;
    @Value("${tradee.file.directory}")
    private String fileDirectory;

//TODO edit this method and use service for repository
    @RequestMapping(value = "/create-trade", method = RequestMethod.POST)
    public ResponseEntity<?> createTrade(@Valid @RequestBody TradeOfferDto tradeOfferDto) {

        Collection<Category> categories = tradeService.findCategoriesByCategoriesDto(tradeOfferDto.getCategoryList());
        County county = tradeService.findCountyByCountyDto(tradeOfferDto.getCountyId());
        Collection<Image> images = tradeService.getImagesByImagesDto(tradeOfferDto.getImageNames());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserPrinciple userPrinciple = (UserPrinciple) auth.getPrincipal();
        UserAccount creator = authService.findUser(userPrinciple.getUsername());
        TradeOffer tradeOffer = new TradeOffer(tradeOfferDto.getTitle(), tradeOfferDto.getDescription(), tradeOfferDto.getSize(), tradeOfferDto.getSex(), categories, county, images, creator);
        images.forEach(i -> i.setTradeOffer(tradeOffer));
        tradeOffer.setStatus("active");//some day status will be usefull
        tradeOfferRepository.save(tradeOffer);
        imageRepository.saveAll(images);
        return new ResponseEntity<>(new ResponseMessage("Trade has been created!"), HttpStatus.OK);
    }

    @RequestMapping(value = "/upload-images", headers = "content-type=multipart/form-data",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> uploadImages(@Valid @RequestParam("file") MultipartFile file,
            @RequestParam("uuid") String uuid) {
        ImageNameInfo imageName = new ImageNameInfo();
        try {
            byte[] bytes = file.getBytes();

            imageName.setName(uuid + file.getOriginalFilename());

            Path path = Paths.get(fileDirectory + imageName.getName());
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
             return new ResponseEntity<>(new ResponseMessage("There was an error in image uploading!"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ResponseMessage("Image has been uploaded!"), HttpStatus.OK);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public Collection<CategoryDto> getCategories() {

        return tradeService.findCategories();
    }

    @RequestMapping(value = "/provinces", method = RequestMethod.GET)
    public List<ProvinceDto> getProvinces() {

        return tradeService.findProvinces();
    }

    @RequestMapping(value = "/trades", method = RequestMethod.GET)
    public Collection<TradeOfferDto> getTrades() {

        return tradeService.findTrades();
    }

    @RequestMapping(value = "/add-to-wishlist", method = RequestMethod.POST)
    public ResponseEntity<?> addToWishlist(@Valid @RequestParam("trade-offer-id") int tradeOfferId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserPrinciple userPrinciple = (UserPrinciple) auth.getPrincipal();
        UserAccount user = authService.findUser(userPrinciple.getUsername());
        tradeService.addToWishList(user, tradeOfferId);

        return new ResponseEntity<>(new ResponseMessage("Link has been sent to your email!"), HttpStatus.OK);
    }

    @RequestMapping(value = "/get-wished-trades", method = RequestMethod.GET)
    public Collection<TradeOfferDto> getWishesOfUser(@Valid @RequestParam("userId") int userId) {
        UserAccount user = authService.findUser(userId);
        return tradeService.findWishesOfUser(user);
    }
    
    @RequestMapping(value = "/get-my-wished-trades", method = RequestMethod.GET)
    public Collection<TradeOfferDto> getWishesOfUser() {
       Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserPrinciple userPrinciple = (UserPrinciple) auth.getPrincipal();
        UserAccount user = authService.findUser(userPrinciple.getUsername());
        return tradeService.findWishesOfUser(user);
    }

}
