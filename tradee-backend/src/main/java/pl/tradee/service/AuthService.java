/*
 * Copyright (c) 2019, Admin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.service;

import pl.tradee.message.request.SignUpForm;
import pl.tradee.model.PasswordResetToken;
import pl.tradee.model.UserAccount;
import pl.tradee.model.VerificationToken;

/**
 *
 * @author Władek
 */
public interface AuthService {
    
    /**
     * Creates new user account
     * @param signUpRequest form with user data
     */
    public void registerNewUser(SignUpForm signUpRequest);
    
    /**
     * Changes user's password
     * @param user user to chacge the password
     * @param newPassword new password
     */
    public void changeUserPassword(UserAccount user,
                                   String newPassword);
    
    /**
     * Generates password reset token to send to the user
     * @param email umail of user to reset the password
     * @return token needed to reset the password
     */
    public PasswordResetToken generateResetPasswordToken(String email);
    
    /**
     * Generates and sends e-mail with password reset token 
     * @param email e-mail address to send the token
     * @param resetToken password reset token
     */
    public void generateChangePasswordMail(String email,
                                           PasswordResetToken resetToken);
    
    /**
     * Activates account of user which received token
     * @param token token received in activation e-mail
     */
    public void confirmMail(VerificationToken token);
    
    /**
     * Finds user with given e-mail
     * @param email e-mail address of the user
     * @return user with given e-mail
     */
    public UserAccount findUser(String email);
    
    /**
     * Finds user with given id
     * @param id user's id
     * @return user with given id
     */
    public UserAccount findUser(long id);
    
    /**
     * Finds user with given phone number
     * @param phone user's phone number
     * @return user with given phone number
     */
    public UserAccount findUser(int phone);
    
    /**
     * Resends e-mail with account confirmation link
     * @param mail user's e-mail address
     */
    public void resendConfirmMailLink(String mail);
    
}
