/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.serviceImpl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.tradee.auth.jwt.JwtProvider;
import pl.tradee.message.request.SignUpForm;
import pl.tradee.model.PasswordResetToken;
import pl.tradee.model.Role;
import pl.tradee.model.RoleName;
import pl.tradee.model.UserAccount;
import pl.tradee.model.VerificationToken;
import pl.tradee.repository.PasswordResetTokenRepository;
import pl.tradee.repository.RoleRepository;
import pl.tradee.repository.UserAccountRepository;
import pl.tradee.repository.VerificationTokenRepository;

/**
 *
 * @author Tomek
 */
@Service
public class AuthServiceImpl {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserAccountRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    EmailServiceImpl emailService;

    public void registerNewUser(SignUpForm signUpRequest) {
        UserAccount user = new UserAccount(signUpRequest.getEmail(),
                signUpRequest.getFirstname(),
                signUpRequest.getLastname(),
                Integer.parseInt(signUpRequest.getPhone()),
                new Date(),
                encoder.encode(signUpRequest.getPassword()));

        user.setEnabled(false);

        Set<Role> roles = new HashSet<>();
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
        roles.add(userRole);
        user.setRoles(roles);

        userRepository.save(user);

        String token = UUID.randomUUID().toString();

        VerificationToken verificationToken = new VerificationToken(token, (60 * 24), user);
        verificationTokenRepository.save(verificationToken);

        emailService.sendSimpleMessage(user.getEmail(),
                "Tradee - Potwierdź swój email!", "Wtiaj "
                + user.getFirstname() + " oto twój link aktywacyjny: "
                + "https://tradee.pl/auth/confirm-mail?token=" + token);
    }

    public void changeUserPassword(UserAccount user, String newPassword) {
        user.setPasshash(encoder.encode(newPassword));
        userRepository.save(user);
    }

    public PasswordResetToken generateResetPasswordToken(String email) {
        UserAccount user = findUser(email);

        String token = UUID.randomUUID().toString();

        PasswordResetToken passwordResetToken = new PasswordResetToken(token, (60 * 24), user);
        passwordResetTokenRepository.save(passwordResetToken);

        return passwordResetToken;
    }

    public void generateChangePasswordMail(String email, PasswordResetToken resetToken) {
        UserAccount user = findUser(email);

        emailService.sendSimpleMessage(email,
                "Zmiana hasła w TRADEE",
                "Witaj " + user.getFirstname() + ", to Twój link do zmiany hasła: "
                + "https://tradee.pl/auth/change-password?id=" + user.getUserId() + "&token=" + resetToken.getToken());

    }

    public void confirmMail(VerificationToken token) {
        UserAccount user = token.getUserId();

        user.setEnabled(true);

        userRepository.save(user);
    }

    public UserAccount findUser(String email) {
        UserAccount user = userRepository.findByEmail(email).orElseThrow(() -> new RuntimeException("Fail! -> Cause: User not find."));
        return user;
    }

    public UserAccount findUser(long id) {
        UserAccount user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("Fail! -> Cause: User not find."));
        return user;
    }

    public UserAccount findUser(int phone) {
        UserAccount user = userRepository.findByPhone(phone).orElseThrow(() -> new RuntimeException("Fail! -> Cause: User not find."));
        return user;
    }

    public void resendConfirmMailLink(String mail) {
        UserAccount user = findUser(mail);

        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = verificationTokenRepository.findByUser(user.getUserId()).orElseThrow(() -> new RuntimeException("Fail! -> Cause: Token not find."));

        verificationToken.setExpiryDateInMinutes(60 * 24);
        verificationToken.setToken(token);

        verificationTokenRepository.save(verificationToken);
        emailService.sendSimpleMessage(user.getEmail(),
                "Nowy link aktywacyjny - Trade",
                "Witaj " + user.getFirstname() + ", to Twój link aktywacyjny: "
                + "https://tradee.pl/auth/confirm-mail?token=" + verificationToken.getToken());
    }
}
