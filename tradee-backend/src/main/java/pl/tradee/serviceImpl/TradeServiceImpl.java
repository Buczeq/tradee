/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.serviceImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tradee.model.Province;
import pl.tradee.repository.CountyRepository;
import pl.tradee.repository.ProvinceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import pl.tradee.message.response.ImageNameInfo;
import pl.tradee.model.Category;
import pl.tradee.model.CategoryDto;
import pl.tradee.model.County;
import pl.tradee.model.CountyDto;
import pl.tradee.model.ProvinceDto;
import pl.tradee.repository.CategoryRepository;
import pl.tradee.model.Image;
import pl.tradee.model.TradeOffer;
import pl.tradee.model.TradeOfferDto;
import pl.tradee.model.UserAccount;
import pl.tradee.model.Wish;
import pl.tradee.repository.ImageRepository;
import pl.tradee.repository.TradeOfferRepository;
import pl.tradee.repository.UserAccountRepository;
import pl.tradee.repository.WishRepository;

/**
 *
 * @author Tomek
 */
@Service
public class TradeServiceImpl {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProvinceRepository provinceRepository;
    @Autowired
    CountyRepository countyRepository;
    @Autowired
    TradeOfferRepository tradeOfferRepository;
    @Autowired
    WishRepository wishRepository;
    @Autowired
    UserAccountRepository userAccountRepository;

    @Autowired
    EmailServiceImpl emailService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    ImageRepository imageRepository;

//Use modelMapper
    public List<ProvinceDto> findProvinces() {
        List<ProvinceDto> provincesDto = new ArrayList<>();
        List<Province> provinces = provinceRepository.findAll();
        for (Province province : provinces) {
            Collection<CountyDto> countiesDto = new ArrayList<>();
            Collection<County> counties = province.getCountyCollection();
            for (County county : counties) {
                CountyDto countyDto = new CountyDto();
                countyDto.setCountyId(county.getCountyId());
                countyDto.setName(county.getName());
                countiesDto.add(countyDto);
            }

            provincesDto.add(new ProvinceDto(province.getProvinceId(), province.getName(), countiesDto));
        }

        return provincesDto;

    }
//TODO change this to lambda
//Use modelMapper

    public Collection<CategoryDto> findCategories() {
        Collection<CategoryDto> categoriesDto = new ArrayList<>();

        Collection<Category> categories = categoryRepository.findAll();
        for (Category category : categories) {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setCategoryId(category.getCategoryId());
            categoryDto.setName(category.getName());
            categoriesDto.add(categoryDto);
        }
        return categoriesDto;
    }

    //TODO check witch Category.name and add not found error handling
    public Collection<Category> findCategoriesByCategoriesDto(Collection<CategoryDto> categoriesDto) {
        Collection<Category> categories = new ArrayList<>();
        categoriesDto.stream()
                .forEach(g -> categories.add(categoryRepository.findByCategoryId(g.getCategoryId()).get()));
        return categories;
    }

    //TODO check witch County.name and add not found error handling
    public County findCountyByCountyDto(CountyDto countyDto) {
        return countyRepository.findByCountyId(countyDto.getCountyId()).get();
    }

//returns list of images
    public Collection<Image> getImagesByImagesDto(Collection<ImageNameInfo> imagesDto) {
        Collection<Image> images = new ArrayList<>();
        imagesDto.forEach(i -> {
                Image image = new Image(new Date(), i.getName());

                images.add(image);
        });
        return images;
    }

    public List<TradeOfferDto> findTrades() {
        List<TradeOfferDto> tradesDto = new ArrayList<>();

        tradeOfferRepository.findAll().forEach(t -> {
            TradeOfferDto tradeDto = modelMapper.map(t, TradeOfferDto.class);
            List<ImageNameInfo> imagesDto = new ArrayList<>();
            Collection<Image> images = t.getImageCollection();
            images.forEach(i -> imagesDto.add(modelMapper.map(i, ImageNameInfo.class)));
            tradeDto.setImageNames(imagesDto);
            tradesDto.add(tradeDto);
        });
        return tradesDto;
    }

    public TradeOffer findTradeOfferById(int id) {
        return tradeOfferRepository.findByTradeOfferId(id).get();
    }

    public void addToWishList(UserAccount user1, int tradeOfferId) {

        TradeOffer tradeOffer = findTradeOfferById(tradeOfferId);
        UserAccount user2 = tradeOffer.getCreator();
        boolean match = checkIfTwoWishesMatches(user1, user2);
        Wish wish = new Wish(tradeOffer, user1);
        wishRepository.save(wish);
        if(match){
            sendMailInformsAboutMatch(user1, user2);
        }

    }

    public boolean checkIfTwoWishesMatches(UserAccount user1, UserAccount user2) {
        List<Wish> wishesOfUser2 = wishRepository.findByUserId(user2).get();
        if (wishesOfUser2 != null && !wishesOfUser2.isEmpty()) {
            for (Wish wish : wishesOfUser2) {
                if (wish.getUserId() == user1) {
                    return true;
                }
            }
        }
        return false;
    }

    public Collection<TradeOfferDto> findWishesOfUser(UserAccount user) {
        List<Wish> wishes = wishRepository.findByUserId(user).get();
        Collection<TradeOfferDto> tradesDto = new ArrayList<>();
        wishes.forEach(w -> {
            tradesDto.add(modelMapper.map(w.getTradeOffer(), TradeOfferDto.class));
        });
        return tradesDto;
    }
    
    public void sendMailInformsAboutMatch(UserAccount user1, UserAccount user2){
        emailService.sendSimpleMessage(user1.getEmail(),
                "Tradee - Możesz dokonać wymiany!", "Wtiaj "
                + user1.getFirstname() + " osoba której przedmiot dodałeś do listy życzeń, posiada twój przedmiot w swojej. " + 
                        "Możesz umówić się na wymianę!" + " Oto mail: " + user2.getEmail() + 
                        " oraz numer telefonu: " + user2.getPhone());
        
        emailService.sendSimpleMessage(user2.getEmail(),
                "Tradee - Możesz dokonać wymiany!", "Wtiaj "
                + user2.getFirstname() + ", osoba której przedmiot dodałeś do listy życzeń, właśnie zrobiła to samo z twoim przedmiotem. " + 
                        "Możesz umówić się na wymianę!" + " Oto mail: " + user1.getEmail() + 
                        " oraz numer telefonu: " + user1.getPhone());
    }

}
