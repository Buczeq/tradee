/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Tomek
 */
@Entity
@Table(name = "county")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "County.findAll", query = "SELECT c FROM County c")
    , @NamedQuery(name = "County.findByCountyId", query = "SELECT c FROM County c WHERE c.countyId = :countyId")
    , @NamedQuery(name = "County.findByProvinceId", query = "SELECT c FROM County c WHERE c.provinceId = :provinceId")
    , @NamedQuery(name = "County.findByName", query = "SELECT c FROM County c WHERE c.name = :name")})
public class County implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @JoinColumn(name = "province_id", referencedColumnName = "province_id")
    @ManyToOne(
            fetch = FetchType.EAGER,optional = false)
    private Province provinceId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "county_id")
    private Integer countyId;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 2147483647)
//    @Column(name = "province")
//    private String province;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "countyId")
//    private List<TradeOffer> tradeOfferList;

    public County() {
    }

    public County(Integer countyId) {
        this.countyId = countyId;
    }

    public County(Integer countyId, Province province, String name) {
        this.countyId = countyId;
        this.provinceId = province;
        this.name = name;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

//    public String getProvince() {
//        return province;
//    }
//
//    public void setProvince(String province) {
//        this.province = province;
//    }


//    @XmlTransient
//    public List<TradeOffer> getTradeOfferList() {
//        return tradeOfferList;
//    }
//
//    public void setTradeOfferList(List<TradeOffer> tradeOfferList) {
//        this.tradeOfferList = tradeOfferList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (countyId != null ? countyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof County)) {
            return false;
        }
        County other = (County) object;
        if ((this.countyId == null && other.countyId != null) || (this.countyId != null && !this.countyId.equals(other.countyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.tradee.model.County[ countyId=" + countyId + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Province getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Province provinceId) {
        this.provinceId = provinceId;
    }
    
}
