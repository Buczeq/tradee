/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import pl.tradee.message.response.ImageNameInfo;

/**
 *
 * @author Tomek
 */
public class TradeOfferDto {
    private int tradeOfferId;
    @NotBlank
    @Size(min = 3, max = 50)
    private String title;
    @NotBlank
    @Size(min = 3, max = 50)
    private String description;
    @NotBlank
    @Size(min = 3, max = 50)
    private String size;
    @NotBlank
    @Size(min = 3, max = 50)
    private String sex;
    
    @NotEmpty
    private Collection<CategoryDto> categoryList;
    @NotNull
    private CountyDto countyId;
    @NotEmpty
    private List<ImageNameInfo> imageCollection;

    public int getTradeOfferId() {
        return tradeOfferId;
    }

    public void setTradeOfferId(int tradeOfferId) {
        this.tradeOfferId = tradeOfferId;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Collection<CategoryDto> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(Collection<CategoryDto> categoryList) {
        this.categoryList = categoryList;
    }

    public CountyDto getCountyId() {
        return countyId;
    }

    public void setCountyId(CountyDto countyId) {
        this.countyId = countyId;
    }

    public List<ImageNameInfo> getImageNames() {
        return imageCollection;
    }

    public void setImageNames(List<ImageNameInfo> imageCollection) {
        this.imageCollection = imageCollection;
    }

   

   
    
}
