/*
 * Copyright (c) 2019, Tomek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pl.tradee.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Tomek
 */
@Entity
@Table(name = "trade_offer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TradeOffer.findAll", query = "SELECT t FROM TradeOffer t")
    , @NamedQuery(name = "TradeOffer.findByTradeOfferId", query = "SELECT t FROM TradeOffer t WHERE t.tradeOfferId = :tradeOfferId")
    , @NamedQuery(name = "TradeOffer.findByTitle", query = "SELECT t FROM TradeOffer t WHERE t.title = :title")
    , @NamedQuery(name = "TradeOffer.findByDescription", query = "SELECT t FROM TradeOffer t WHERE t.description = :description")
    , @NamedQuery(name = "TradeOffer.findByStatus", query = "SELECT t FROM TradeOffer t WHERE t.status = :status")})
public class TradeOffer implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "size")
    private String size;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "sex")
    private String sex;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "trade_offer_has_category", 
    	joinColumns = @JoinColumn(name = "trade_offer_id"), 
    	inverseJoinColumns = @JoinColumn(name = "category_id"))
    private Collection<Category> categoryList;
    @JoinColumn(name = "county_id", referencedColumnName = "county_id")
    @ManyToOne(optional = false)
    private County countyId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trade_offer_id")
    private Integer tradeOfferId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tradeOffer")
    private Collection<Wish> wishCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tradeOffer")
//    @JoinColumn(name = "image", referencedColumnName = "image_id")
        @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "tradeOffer")
@Fetch(value = FetchMode.SUBSELECT)
    private Collection<Image> imageCollection;
    @JoinColumn(name = "creator", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private UserAccount creator;

    public TradeOffer() {
    }

    public TradeOffer(Integer tradeOfferId) {
        this.tradeOfferId = tradeOfferId;
    }

    public TradeOffer(Integer tradeOfferId, String title, String description, String status) {
        this.tradeOfferId = tradeOfferId;
        this.title = title;
        this.description = description;
        this.status = status;
    }

    public TradeOffer(String title, String description, String size, String sex, Collection<Category> categoryList, County countyId, Collection<Image> imageCollection, UserAccount creator) {
        this.title = title;
        this.description = description;
        this.size = size;
        this.sex = sex;
        this.categoryList = categoryList;
        this.countyId = countyId;
        this.imageCollection = imageCollection;
        this.creator = creator;
    }
    
    

    public Integer getTradeOfferId() {
        return tradeOfferId;
    }

    public void setTradeOfferId(Integer tradeOfferId) {
        this.tradeOfferId = tradeOfferId;
    }


    @XmlTransient
    public Collection<Wish> getWishCollection() {
        return wishCollection;
    }

    public void setWishCollection(Collection<Wish> wishCollection) {
        this.wishCollection = wishCollection;
    }

    @XmlTransient
    public Collection<Image> getImageCollection() {
        return imageCollection;
    }

    public void setImageCollection(Collection<Image> imageCollection) {
        this.imageCollection = imageCollection;
    }

    public UserAccount getCreator() {
        return creator;
    }

    public void setCreator(UserAccount creator) {
        this.creator = creator;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tradeOfferId != null ? tradeOfferId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TradeOffer)) {
            return false;
        }
        TradeOffer other = (TradeOffer) object;
        if ((this.tradeOfferId == null && other.tradeOfferId != null) || (this.tradeOfferId != null && !this.tradeOfferId.equals(other.tradeOfferId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.tradee.model.TradeOffer[ tradeOfferId=" + tradeOfferId + " ]";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @XmlTransient
    public Collection<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(Collection<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public County getCountyId() {
        return countyId;
    }

    public void setCountyId(County countyId) {
        this.countyId = countyId;
    }
    
}
